package net.avcompris.devops;

import static com.google.common.base.Preconditions.checkState;
import static com.google.common.collect.Lists.newArrayList;
import static com.google.common.collect.Maps.newHashMap;
import static java.nio.charset.StandardCharsets.UTF_8;
import static org.apache.commons.lang3.StringUtils.substringAfter;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assumptions.assumeFalse;
import static org.junit.jupiter.api.Assumptions.assumeTrue;

import java.io.File;
import java.io.IOException;
import java.util.Collection;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.stream.Stream;

import javax.annotation.Nullable;

import org.apache.commons.io.FileUtils;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import net.avcompris.binding.dom.helper.DomBinderUtils;
import net.avcompris.devops.Pom.Dependency;
import net.avcompris.devops.Pom.License;
import net.avcompris.devops.Pom.Plugin;
import net.avcompris.devops.Pom.Repository;
import net.avcompris.devops.Pom.Site;

public class AllPomsTest {

	@ParameterizedTest(name = "{0}")
	@MethodSource("dirsAndPoms")
	public void testModelVersion(final String dirName, final File dir, final Pom pom) throws Exception {

		assertEquals("4.0.0", pom.getModelVersion());
	}

	@ParameterizedTest(name = "{0}")
	@MethodSource("dirsAndPoms")
	public void testArtifactIdIsProjectName(final String dirName, final File dir, final Pom pom) throws Exception {

		final String projectName = pom.getName();

		assertEquals(projectName, pom.getArtifactId());
	}

	@ParameterizedTest(name = "{0}")
	@MethodSource("dirsAndPoms")
	public void testProjectNameIsDirName(final String dirName, final File dir, final Pom pom) throws Exception {

		final String projectName = pom.getName();

		assertEquals(dirName, projectName);
	}

	@ParameterizedTest(name = "{0}")
	@MethodSource("dirsAndPoms")
	public void testReadmeHeading1IsProjectName(final String dirName, final File dir, final Pom pom) throws Exception {

		assumeNotModule(dir);

		final String projectName = pom.getName();

		final File readmeFile = new File(dir, "README.md");

		final String header1 = extractHeader1FromMarkdownFile(readmeFile);

		assertEquals("About " + projectName, header1);
	}

	private static String extractHeader1FromMarkdownFile(final File mdFile) throws IOException {

		final String line0 = FileUtils.readLines(mdFile, UTF_8.name()).get(0);

		if (!line0.startsWith("# ")) {

			throw new RuntimeException("line0 should start with \"# \", but was: " + line0);
		}

		return substringAfter(line0, "# ").trim();
	}

	@ParameterizedTest(name = "{0}")
	@MethodSource("dirsAndPoms")
	public void testUrl(final String dirName, final File dir, final Pom pom) throws Exception {

		final String projectName = pom.getName();

		assertEquals("https://maven.avcompris.com/" + projectName + "/", pom.getUrl());

		// https://maven.avcompris.com/avc-commons3-all/
	}

	@ParameterizedTest(name = "{0}")
	@MethodSource("dirsAndPoms")
	public void testScmConnection(final String dirName, final File dir, final Pom pom) throws Exception {

		assumeNotModule(dir);

		final String projectName = pom.getName();

		assertEquals("scm:git:https://gitlab.com/avcompris/" + projectName + ".git", pom.getScm().getConnection());
	}

	@ParameterizedTest(name = "{0}")
	@MethodSource("dirsAndPoms")
	public void testScmDeveloperConnection(final String dirName, final File dir, final Pom pom) throws Exception {

		assumeNotModule(dir);

		final String projectName = pom.getName();

		assertEquals("scm:git:https://gitlab.com/avcompris/" + projectName + ".git", pom.getScm().getConnection());
	}

	@ParameterizedTest(name = "{0}")
	@MethodSource("dirsAndPoms")
	public void testScmUrl(final String dirName, final File dir, final Pom pom) throws Exception {

		assumeNotModule(dir);

		final String projectName = pom.getName();

		assertEquals("https://gitlab.com/avcompris/" + projectName, pom.getScm().getUrl());
	}

	@ParameterizedTest(name = "{0}")
	@MethodSource("dirsAndPoms")
	public void testCiManagementSystem(final String dirName, final File dir, final Pom pom) throws Exception {

		assumeNotModule(dir);
		
		assertEquals("gitlab", pom.getCiManagement().getSystem());
	}

	private static void assumeNotModule(final File dir) throws IOException {

		if (!new File(dir, ".git").isDirectory()) {

			final File parentDir = dir.getParentFile();

			if (new File(parentDir, ".git").isDirectory()) {

				final File parentPomFile = new File(parentDir, "pom.xml");

				if (parentPomFile.isFile()) {

					final Pom parentPom = DomBinderUtils.xmlContentToJava(parentPomFile, Pom.class);

					assumeFalse(hasModule(parentPom, dir.getName()));
				}
			}
		}
	}

	private static boolean hasModule(final Pom pom, final String moduleName) {

		for (final String module : pom.getModules()) {

			if (module.equals(moduleName)) {

				return true;
			}
		}

		return false;
	}

	@ParameterizedTest(name = "{0}")
	@MethodSource("dirsAndPoms")
	public void testCiManagementUrl(final String dirName, final File dir, final Pom pom) throws Exception {

		assumeNotModule(dir);

		final String projectName = pom.getName();

		assertEquals("https://gitlab.com/avcompris/" + projectName + "/-/pipelines", pom.getCiManagement().getUrl());
	}

	@ParameterizedTest(name = "{0}")
	@MethodSource("dirsAndPoms")
	public void testJavadocLinks(final String dirName, final File dir, final Pom pom) throws Exception {

		final Plugin javadocPlugin = pom.getPlugin("maven-javadoc-plugin");

		assumeTrue(javadocPlugin != null);

		assertEquals("UTF-8", javadocPlugin.getConfiguration().getEncoding());

		for (final String link : javadocPlugin.getConfiguration().getLinks()) {

			if (!link.startsWith("https://maven.avcompris.com/")) {

				assertEquals("https://docs.oracle.com/javase/8/docs/api/", link);
			}
		}
	}

	@ParameterizedTest(name = "{0}")
	@MethodSource("dirsAndPoms")
	public void testOssrhRepository(final String dirName, final File dir, final Pom pom) throws Exception {

		assumeTrue(pom.getModules() == null || pom.getModules().length == 0);

		final Repository repository = pom.getRepository("OSSRH");

		assertEquals("https://oss.sonatype.org/content/repositories/snapshots", repository.getUrl());

		assertFalse(repository.hasReleasesEnabled());
		assertTrue(repository.hasSnapshotsEnabled());
	}

	@ParameterizedTest(name = "{0}")
	@MethodSource("dirsAndPoms")
	public void testAvcomprisDependencyVersions(final String dirName, final File dir, final Pom pom) throws Exception {

		checkAvcomprisDependencyVersion(pom, pom.getParent());

		for (final Dependency dependency : pom.getDependencies()) {

			checkAvcomprisDependencyVersion(pom, dependency);
		}
	}

	private static void checkAvcomprisDependencyVersion(final Pom pom, @Nullable final Dependency dependency) {

		if (dependency == null || !"net.avcompris.commons".equals(dependency.getGroupId())) {

			return;
		}

		final String artifactId = dependency.getArtifactId();
		final String version = dependency.getVersion();

		final Pom originalPom = ALL_POMS.get(artifactId);

		checkState(originalPom != null, "Cannot find POM for artifactId: " + artifactId);

		assertEquals(originalPom.getVersion(), version,
				pom.getArtifactId() + ".dependency: " + artifactId + ".version");
	}

	@ParameterizedTest(name = "{0}")
	@MethodSource("dirsAndPoms")
	public void testDistributionManagementSite(final String dirName, final File dir, final Pom pom) throws Exception {

		assumeTrue(pom.getModules() == null || pom.getModules().length == 0);

		final String projectName = pom.getName();

		final Site site = pom.getDistributionManagement().getSite();

		assertEquals("avcompris-sites", site.getId());

		assertEquals("scp://maven.avcompris.com/" + projectName + "/", site.getUrl());
	}

	@ParameterizedTest(name = "{0}")
	@MethodSource("dirsAndPoms")
	public void testLicense(final String dirName, final File dir, final Pom pom) throws Exception {

		assumeNotModule(dir);

		assertEquals(1, pom.getLicenses().length);

		final License license = pom.getLicenses()[0];

		assertEquals("Apache License Version 2.0", license.getName());
		assertEquals("http://www.apache.org/licenses/LICENSE-2.0.txt", license.getUrl());
		assertEquals("repo, manual", license.getDistribution());
		assertEquals("A business-friendly OSS license", license.getComments());
	}

	@ParameterizedTest(name = "{0}")
	@MethodSource("dirsAndPoms")
	public void testIndexAptContainsLinkToGitLabHomePage(final String dirName, final File dir, final Pom pom)
			throws Exception {

		final String projectName = pom.getName();

		final File aptFile = new File(dir, "src/site/apt/index.apt");

		assumeTrue(aptFile.isFile());

		final Link[] links = AptUtils.extractLinksFromApt(aptFile);

		final Link link = LinkUtils.getLinkByText(links, "project home page");

		assertEquals("https://gitlab.com/avcompris/" + projectName + "/", link.getUrl());
	}

	@ParameterizedTest(name = "{0}")
	@MethodSource("dirsAndPoms")
	public void testReadmeMdContainsLinkToMavenGeneratedSite(final String dirName, final File dir, final Pom pom)
			throws Exception {

		assumeNotModule(dir);

		assumeTrue(pom.getModules().length == 0);

		final String projectName = pom.getName();

		final Link[] links = MarkdownUtils.extractLinksFromMarkdown(new File(dir, "README.md"));

		final Link link = LinkUtils.getLinkByText(links, "Maven Generated Site.");

		assertEquals("https://maven.avcompris.com/" + projectName + "/", link.getUrl());
	}

	@ParameterizedTest(name = "{0}")
	@MethodSource("dirsAndPoms")
	public void testReadmeMdContainsLinkToJavadoc(final String dirName, final File dir, final Pom pom)
			throws Exception {

		assumeNotModule(dir);

		assumeFalse("pom".equals(pom.getPackaging()));

		final String projectName = pom.getName();

		assumeFalse(projectName.endsWith("-ut"));

		final Link[] links = MarkdownUtils.extractLinksFromMarkdown(new File(dir, "README.md"));

		final Link link = LinkUtils.getLinkByText(links, "API Documentation is here");

		// e.g. [API Documentation is here](https://maven.avcompris.com/avc-commons3-types/apidocs/index.html).

		assertEquals("https://maven.avcompris.com/" + projectName + "/apidocs/index.html", link.getUrl());
	}

	@ParameterizedTest(name = "{0}")
	@MethodSource("dirsAndPoms")
	public void testNoOccurrenceOfBitbucket(final String dirName, final File dir, final Pom pom) throws Exception {

		assumeNotModule(dir);

		final File aptFile = new File(dir, "src/site/apt/index.apt");

		if (aptFile.isFile()) {

			assertFalse(FileUtils.readFileToString(aptFile, UTF_8.name()) //
					.toLowerCase(Locale.ENGLISH) //
					.contains("bitbucket"), "The .apt file should not contain: bitbucket");
		}

		final File mdFile = new File(dir, "README.md");

		assertFalse(FileUtils.readFileToString(mdFile, UTF_8.name()) //
				.toLowerCase(Locale.ENGLISH) //
				.contains("bitbucket"), "The .md file should not contain: bitbucket");
	}

	@ParameterizedTest(name = "{0}")
	@MethodSource("dirsAndPoms")
	public void testProjectLinksInReadmeMd(final String dirName, final File dir, final Pom pom) throws Exception {

		assumeNotModule(dir);

		final Link[] links = MarkdownUtils.extractLinksFromMarkdown(new File(dir, "README.md"));

		for (final Link link : links) {

			if ("Maven Generated Site.".equals(link.getText())) {
				continue;
			}

			final String linkProjectName = link.getText();

			if (!ALL_POMS.containsKey(linkProjectName)) {
				continue;
			}

			assertEquals("https://gitlab.com/avcompris/" + linkProjectName + "/", link.getUrl());
		}
	}

	@ParameterizedTest(name = "{0}")
	@MethodSource("dirsAndPoms")
	public void testOtherLinksAreIdenticalInIndexAptAndInReadmeMd(final String dirName, final File dir, final Pom pom)
			throws Exception {

		final File aptFile = new File(dir, "src/site/apt/index.apt");

		assumeTrue(aptFile.isFile());

		final Link[] aptLinks = AptUtils.extractLinksFromApt(aptFile);

		final Link[] mdLinks = MarkdownUtils.extractLinksFromMarkdown(new File(dir, "README.md"));

		for (final Link aptLink : aptLinks) {

			if ("project home page".equals(aptLink.getText())) {
				continue;
			}

			final String mdLinkRef = aptLink.getUrl().startsWith("./") //
					? ("https://maven.avcompris.com/" + dirName + aptLink.getUrl().substring(1)) //
					: aptLink.getUrl();

			final Link mdLink = LinkUtils.getLinkByText(mdLinks, aptLink.getText());

			assertEquals(mdLinkRef, mdLink.getUrl());
		}

		for (final Link mdLink : mdLinks) {

			if ("Maven Generated Site.".equals(mdLink.getText())) {
				continue;
			}

			final String aptLinkRef = mdLink.getUrl().startsWith("https://maven.avcompris.com/" + dirName + "/") //
					? ("./" + substringAfter(mdLink.getUrl(), dirName + "/")) //
					: mdLink.getUrl();

			final Link aptLink = LinkUtils.getLinkByText(aptLinks, mdLink.getText());

			assertEquals(aptLinkRef, aptLink.getUrl());
		}
	}

	private static final Map<String, Pom> ALL_POMS = newHashMap();

	private static List<Arguments> DIRS_AND_POMS = null;

	private static Stream<Arguments> dirsAndPoms() throws Exception {

		if (DIRS_AND_POMS != null) {

			return DIRS_AND_POMS.stream();
		}

		final File allPomDir = new File("../avc-commons3-all");

		final File allPomFile = new File(allPomDir, "pom.xml");

		final Pom allPom = DomBinderUtils.xmlContentToJava(allPomFile, Pom.class);

		ALL_POMS.put(allPomDir.getName(), allPom);

		final List<Arguments> arguments = newArrayList();

		arguments.add(Arguments.of(allPomDir.getName(), allPomDir, allPom));

		addPomModules(arguments, allPomDir, allPom);

		DIRS_AND_POMS = arguments;

		return arguments.stream();
	}

	private static void addPomModules(final Collection<Arguments> arguments, final File pomDir, final Pom pom)
			throws Exception {

		for (final String module : pom.getModules()) {

			final File moduleDir = new File(pomDir, module);

			final File modulePomFile = new File(moduleDir, "pom.xml");

			final Pom modulePom = DomBinderUtils.xmlContentToJava(modulePomFile, Pom.class);

			ALL_POMS.put(moduleDir.getName(), modulePom);

			arguments.add(Arguments.of(moduleDir.getName(), moduleDir, modulePom));

			addPomModules(arguments, moduleDir, modulePom);
		}
	}
}
