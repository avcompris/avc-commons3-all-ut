package net.avcompris.devops;

import javax.annotation.Nullable;

import net.avcompris.binding.annotation.Namespaces;
import net.avcompris.binding.annotation.XPath;

@Namespaces("xmlns:pom=http://maven.apache.org/POM/4.0.0")
@XPath("/pom:project")
public interface Pom {

	@XPath("pom:modelVersion")
	String getModelVersion();

	@Nullable
	@XPath("pom:groupId")
	String getGroupId();

	@XPath("pom:artifactId")
	String getArtifactId();

	@Nullable
	@XPath("pom:version")
	String getVersion();

	@Nullable
	@XPath("pom:packaging")
	String getPackaging();

	@Nullable
	@XPath("pom:parent")
	Parent getParent();

	interface Parent extends Dependency {

		@XPath("pom:groupId")
		String getGroupId();

		@XPath("pom:artifactId")
		String getArtifactId();

		@XPath("pom:version")
		String getVersion();
	}

	@XPath("pom:name")
	@Nullable
	String getName();

	@XPath("pom:url")
	@Nullable
	String getUrl();

	@XPath("pom:scm")
	@Nullable
	Scm getScm();

	interface Scm {

		@XPath("pom:connection")
		String getConnection();

		@XPath("pom:developerConnection")
		String getDeveloperConnection();

		@XPath("pom:url")
		String getUrl();
	}

	@XPath("pom:ciManagement")
	@Nullable
	CiManagement getCiManagement();

	interface CiManagement {

		@XPath("pom:system")
		String getSystem();

		@XPath("pom:url")
		String getUrl();
	}

	@Nullable
	@XPath("pom:modules/pom:module")
	String[] getModules();

	@XPath("pom:dependencies/pom:dependency")
	Dependency[] getDependencies();

	interface Dependency {

		@XPath("pom:groupId")
		String getGroupId();

		@XPath("pom:artifactId")
		String getArtifactId();

		@Nullable
		@XPath("pom:version")
		String getVersion();
	}

	@XPath("pom:repositories/pom:repository")
	Repository[] getRepositories();

	@XPath("pom:repositories/pom:repository[pom:id = $arg0]")
	Repository getRepository(String id);

	interface Repository {

		@XPath("pom:id")
		String getId();

		@XPath("pom:url")
		String getUrl();

		@XPath("pom:releases/pom:enabled = 'true'")
		boolean hasReleasesEnabled();

		@XPath("pom:snapshots/pom:enabled = 'true'")
		boolean hasSnapshotsEnabled();
	}

	@XPath("pom:distributionManagement")
	DistributionManagement getDistributionManagement();

	interface DistributionManagement {

		@XPath("pom:site")
		Site getSite();
	}

	interface Site {

		@XPath("pom:id")
		String getId();

		@XPath("pom:url")
		String getUrl();
	}

	@XPath("pom:reporting/pom:plugins/pom:plugin")
	Plugin[] getReportingPlugins();

	@XPath("pom:reporting/pom:plugins/pom:plugin[pom:artifactId = $arg0]")
	@Nullable
	Plugin getPlugin(String artifactId);

	@XPath("pom:reporting/pom:plugins/pom:plugin[pom:artifactId = $arg0]")
	boolean isNullPlugin(String artifactId);

	interface Plugin {

		@XPath("pom:groupId")
		String getGroupId();

		@XPath("pom:artifactId")
		String getArtifactId();

		@XPath("pom:configuration")
		Configuration getConfiguration();
	}

	interface Configuration {

		@XPath("pom:encoding")
		String getEncoding();

		@XPath("pom:links/pom:link")
		String[] getLinks();
	}

	@Nullable
	@XPath("pom:licenses/pom:license")
	License[] getLicenses();

	interface License {

		@XPath("pom:name")
		String getName();

		@XPath("pom:url")
		String getUrl();

		@XPath("pom:distribution")
		String getDistribution();

		@XPath("pom:comments")
		String getComments();
	}
}
