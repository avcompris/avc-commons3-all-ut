package net.avcompris.devops;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.collect.Lists.newArrayList;
import static java.nio.charset.StandardCharsets.UTF_8;
import static net.avcompris.commons3.databeans.DataBeans.instantiate;
import static org.apache.commons.lang3.StringUtils.substringAfter;
import static org.apache.commons.lang3.StringUtils.substringBetween;

import java.io.File;
import java.io.IOException;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.NotImplementedException;

import com.google.common.collect.Iterables;

public abstract class AptUtils {

	public static Link[] extractLinksFromApt(final File aptFile) throws IOException {

		checkNotNull(aptFile, "aptFile");

		final List<Link> links = newArrayList();

		for (final String line : FileUtils.readLines(aptFile, UTF_8.name())) {

			// e.g. "{{{https://gitlab.com/avcompris/avc-base-testutil-ut/}project home page}}"
			//
			if (line.contains("{{{")) {

				final String url = substringBetween(line, "{{{", "}");

				final String text = substringBetween(line, "}", "}}");

				links.add(instantiate(Link.class) //
						.setUrl(url) //
						.setText(text));

				if (substringAfter(line, "}}").contains("}")) {

					throw new NotImplementedException("line: " + line);
				}
			}
		}

		return Iterables.toArray(links, Link.class);
	}
}
