package net.avcompris.devops;

public abstract class LinkUtils {

	public static Link getLinkByText(final Link[] links, final String text) {

		for (final Link link : links) {

			if (link.getText().equals(text)) {

				return link;
			}
		}

		throw new IllegalArgumentException("text: " + text);
	}
}
