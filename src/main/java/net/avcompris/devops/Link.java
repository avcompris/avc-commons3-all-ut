package net.avcompris.devops;

public interface Link {

	String getText();

	String getUrl();

	Link setText(String text);

	Link setUrl(String urt);
}
