# About avc-commons3-all-ut


Devops stuff for **avc-commons** projects.

Relies on 
[avc-commons3-all](https://gitlab.com/avcompris/avc-commons3-all/) to get the list
of all projects.


There is a [Maven Generated Site.](https://maven.avcompris.com/avc-commons3-all-ut/)
